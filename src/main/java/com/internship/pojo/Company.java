package com.internship.pojo;

/**
 * @author Yordan Marinov
 */
public class Company {
    private int companyID;
    private String name;
    private String town;
    private String phoneNumber;
    private int foundingYear;
    private int representativeID;

    public Company()
    {}

    public Company(int company_id,
                   String name,
                   String town,
                   String phoneNumber,
                   int registerYear,
                   int representative_id) {
        this.companyID = company_id;
        this.name = name;
        this.town = town;
        this.phoneNumber = phoneNumber;
        this.foundingYear = registerYear;
        this.representativeID = representative_id;
    }

    public int getCompanyID() {
        return this.companyID;
    }

    public void setCompanyID(int company_id) {
        this.companyID = company_id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTown() {
        return this.town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getFoundingYear() {
        return this.foundingYear;
    }

    public void setFoundingYear(int registerYear) {
        this.foundingYear = registerYear;
    }

    public int getRepresentativeID() {
        return this.representativeID;
    }

    public void setRepresentativeID(int representative_id) {
        this.representativeID = representative_id;
    }

    //DELETE

    @Override
    public String toString() {
        return "Company{" +
                "companyID=" + companyID +
                ", name='" + name + '\'' +
                ", town='" + town + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", foundingYear=" + foundingYear +
                ", representativeID=" + representativeID +
                '}';
    }
}
