package com.internship.services;

import com.internship.dao.Dao;
import com.internship.dao.RepresentativeDaoImpl;
import com.internship.pojo.Representative;

import java.util.List;

/**
 * @author Yordan Marinov
 */
public class RepresentativeServiceImpl implements Service<Representative> {
    private Dao<Representative> dao = new RepresentativeDaoImpl();

    @Override
    public List<Representative> getAllEntities() {
        return dao.getAll();
    }

    @Override
    public Representative getEntityById(int id) {
        return dao.getById(id);
    }

    @Override
    public int saveEntity(Representative toSave) {
        return dao.save(toSave);
    }

    @Override
    public int updateEntity(Representative toUpdate) {
        return dao.update(toUpdate);
    }

    @Override
    public int deleteEntity(Representative toDelete) {
        return dao.delete(toDelete);
    }
}
