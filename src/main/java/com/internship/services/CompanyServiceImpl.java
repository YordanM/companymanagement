package com.internship.services;

import com.internship.dao.CompanyDaoImpl;
import com.internship.dao.Dao;
import com.internship.pojo.Company;

import java.util.List;

/**
 * @author Yordan Marinov
 */
public class CompanyServiceImpl implements Service<Company> {
    private Dao<Company> dao = new CompanyDaoImpl();

    @Override
    public List<Company> getAllEntities() {
        return dao.getAll();
    }

    @Override
    public Company getEntityById(int id) {
        return dao.getById(id);
    }

    @Override
    public int saveEntity(Company toSave) {
        return dao.save(toSave);
    }

    @Override
    public int updateEntity(Company toUpdate) {
        return dao.update(toUpdate);
    }

    @Override
    public int deleteEntity(Company toDelete) {
        return dao.delete(toDelete);
    }
}
