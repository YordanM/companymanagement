package com.internship.services;

import java.util.List;

/**
 * @author Yordan Marinov
 */
public interface Service<E> {
    List<E> getAllEntities();
    E getEntityById(int id);
    int saveEntity(E toSave);
    int updateEntity(E toUpdate);
    int deleteEntity(E toDelete);
}
