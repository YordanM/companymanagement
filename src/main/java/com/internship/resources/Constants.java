package com.internship.resources;

/**
 * @author Yordan Marinov
 */
public class Constants {
    public static final String url = "jdbc:postgresql://localhost:5432/manager";
    public static final String username = "admin";
    public static final String password = "admin";
    public static final String sqlGetAllCompanies = "SELECT * FROM manager.company;";
    public static final String sqlGetCompanyById = "SELECT * FROM manager.company WHERE company_id =?;";
    public static final String sqlSaveCompany =
            "INSERT INTO manager.company " +
                    " (name," +
                    " town," +
                    " phone_number," +
                    " founding_year," +
                    " representative_id," +
                    " company_id) " +
                    " VALUES (?, ?, ?, ?, ?, ?);";
    public static final String sqlUpdateCompany =
            "UPDATE manager.company SET " +
                    "name=?," +
                    " town=?," +
                    " phone_number=?," +
                    " founding_year=?," +
                    " representative_id=?" +
                    " WHERE company_id=?;";
    public static final String sqlDeleteCompany = "DELETE FROM manager.company WHERE company_id =?;";
    public static final String sqlGetAllRepresentatives = "SELECT * FROM manager.representative;";
    public static final String sqlGetRepresentativeById =
            "SELECT * FROM manager.representative WHERE representative_id =?;";
    public static final String sqlSaveRepresentative =
            "INSERT INTO manager.representative " +
                    "(first_name," +
                    " middle_name," +
                    " last_name," +
                    " representative_phone_number," +
                    " email_address," +
                    " representative_id)" +
                    " VALUES (?, ?, ?, ?, ?, ?);";
    public static final String sqlUpdateRepresentative =
            "UPDATE manager.representative SET " +
                    "first_name=?," +
                    " middle_name=?," +
                    " last_name=?," +
                    " representative_phone_number=?," +
                    " email_address=? WHERE representative_id=?;";
    public static final String sqlDeleteRepresentative =
            "DELETE FROM manager.representative WHERE representative_id =?;";
}
