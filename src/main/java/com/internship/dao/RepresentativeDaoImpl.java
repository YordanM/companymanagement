package com.internship.dao;

import com.internship.pojo.Representative;
import com.internship.resources.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yordan Marinov
 */
public class RepresentativeDaoImpl extends DaoImpl<Representative> {

    private void fillRepresentative(Representative current, ResultSet result) throws SQLException {
        current.setRepresentativeID(result.getInt("representative_id"));
        current.setFirstName(result.getString("first_name"));
        current.setMiddleName(result.getString("middle_name"));
        current.setLastName(result.getString("last_name"));
        current.setPhoneNumber(result.getString("representative_phone_number"));
        current.setEmailAddress(result.getString("email_address"));
    }

    @Override
    public List<Representative> getAll() {
        List<Representative> representativeList = new ArrayList<>();
        try {
            Connection connection = getConnection();
            PreparedStatement statement =
                    connection.prepareStatement(Constants.sqlGetAllRepresentatives);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Representative current = new Representative();
                fillRepresentative(current, result);
                representativeList.add(current);
            }
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        if (!representativeList.isEmpty())
            logger.info("Successfully retrieved all entities.");
        else
            logger.info("No entity retrieved.");
        return representativeList;
    }

    @Override
    public Representative getById(int id) {
        Representative current = null;
        try {
            Connection connection = getConnection();
            PreparedStatement statement =
                    connection.prepareStatement(Constants.sqlGetRepresentativeById);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                current = new Representative();
                fillRepresentative(current, result);
            }
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        if (current != null)
            logger.info("Successfully retrieved an entity.");
        else
            logger.info("No entity retrieved.");
        return current;
    }

    private void setColumns(Representative toSet, PreparedStatement statement) throws SQLException {
        statement.setInt(6, toSet.getRepresentativeID());
        statement.setString(1, toSet.getFirstName());
        statement.setString(2, toSet.getMiddleName());
        statement.setString(3, toSet.getLastName());
        statement.setString(4, toSet.getPhoneNumber());
        statement.setString(5, toSet.getEmailAddress());
    }

    @Override
    public int save(Representative toSave) {
        int status;
        try {
            Connection connection = getConnection();
            PreparedStatement statement =
                    connection.prepareStatement(Constants.sqlSaveRepresentative);
            setColumns(toSave, statement);
            if (statement.executeUpdate() != 0) {
                status = 200;
                logger.info("Successfully saved an entity.");
            } else {
                status = 304;
                logger.info("No changes made to entity.");
            }
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            status = 400;
        }
        return status;
    }

    @Override
    public int update(Representative toUpdate) {
        int status;
        try {
            Connection connection = getConnection();
            PreparedStatement statement =
                    connection.prepareStatement(Constants.sqlUpdateRepresentative);
            setColumns(toUpdate, statement);
            if (statement.executeUpdate() != 0) {
                status = 200;
                logger.info("Successfully updated an entity.");
            } else {
                status = 304;
                logger.info("No changes made to entity.");
            }
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            status = 400;
        }
        return status;
    }

    @Override
    public int delete(Representative toDelete) {
        int status;
        try {
            Connection connection = getConnection();
            PreparedStatement statement =
                    connection.prepareStatement(Constants.sqlDeleteRepresentative);
            statement.setObject(1, toDelete.getRepresentativeID()); //
            if (statement.executeUpdate() != 0) {
                status = 200;
                logger.info("Successfully deleted an entity.");
            } else {
                status = 304;
                logger.info("No changes made to entity.");
            }
            connection.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
            status = 400;
        }
        return status;
    }
}