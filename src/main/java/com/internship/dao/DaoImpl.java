package com.internship.dao;

import com.internship.resources.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Yordan Marinov
 */
public abstract class DaoImpl<E> implements Dao<E> {

    static final Logger logger
            = LoggerFactory.getLogger(CompanyDaoImpl.class);

    public abstract List<E> getAll();

    public abstract E getById(int id);

    public abstract int save(E toSave);

    public abstract int update(E toUpdate);

    public abstract int delete(E toDelete);

    static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(Constants.url, Constants.username, Constants.password);
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
        return connection;
    }

}
