package com.internship.dao;

import com.internship.pojo.Company;
import com.internship.resources.Constants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yordan Marinov
 */
public class CompanyDaoImpl extends DaoImpl<Company> {

    private void fillCompany(Company current, ResultSet result) throws SQLException {
        current.setCompanyID(result.getInt("company_id"));
        current.setName(result.getString("name"));
        current.setTown(result.getString("town"));
        current.setPhoneNumber(result.getString("phone_number"));
        current.setFoundingYear(result.getInt("founding_year"));
        current.setRepresentativeID(result.getInt("representative_id"));
    }

    @Override
    public List<Company> getAll() {
        List<Company> representativeList = new ArrayList<>();
        try {
            Connection connection = getConnection();
            PreparedStatement statement =
                    connection.prepareStatement(Constants.sqlGetAllCompanies);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Company current = new Company();
                fillCompany(current, result);
                representativeList.add(current);
            }
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        if (!representativeList.isEmpty())
            logger.info("Successfully retrieved all entities.");
        else
            logger.info("No entity retrieved.");
        return representativeList;
    }

    @Override
    public Company getById(int id) {
        Company current = null;
        try {
            Connection connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(Constants.sqlGetCompanyById);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                current = new Company();
                fillCompany(current, result);
            }
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        if (current != null)
            logger.info("Successfully retrieved an entity.");
        else
            logger.info("No entity retrieved.");
        return current;
    }

    private void setColumns(Company toSet, PreparedStatement statement) throws SQLException {
        statement.setInt(6, toSet.getCompanyID());
        statement.setString(1, toSet.getName());
        statement.setString(2, toSet.getTown());
        statement.setString(3, toSet.getPhoneNumber());
        statement.setInt(4, toSet.getFoundingYear());
        if (recordExists(toSet.getRepresentativeID()))
            statement.setInt(5, toSet.getRepresentativeID());
        else
            statement.setObject(5, null);
    }

    private boolean recordExists(int id) {
        boolean exists = false;
        try {
            Connection connection = getConnection();
            String sqlCheck = "SELECT * FROM manager.representative WHERE representative_id=?;";
            PreparedStatement checkRep =
                    connection.prepareStatement(sqlCheck);
            checkRep.setInt(1, id);
            ResultSet result = checkRep.executeQuery();
            exists = result.next();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return exists;
    }

    @Override
    public int save(Company toSave) {
        int status;
        try {
            Connection connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(Constants.sqlSaveCompany);
            setColumns(toSave, statement);
            if (statement.executeUpdate() != 0) {
                status = 200;
                logger.info("Successfully saved an entity.");
            } else {
                status = 304;
                logger.info("No changes made to entity.");
            }
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            status = 400;
        }
        return status;
    }

    @Override
    public int update(Company toUpdate) {
        int status;
        try {
            Connection connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(Constants.sqlUpdateCompany);
            setColumns(toUpdate, statement);
            if (statement.executeUpdate() != 0) {
                status = 200;
                logger.info("Successfully updated an entity.");
            } else {
                status = 304;
                logger.info("No changes made to entity.");
            }
            connection.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            status = 400;
        }
        return status;
    }

    @Override
    public int delete(Company toDelete) {
        int status;
        try {
            Connection connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(Constants.sqlDeleteCompany);
            statement.setInt(1, toDelete.getCompanyID());
            if (statement.executeUpdate() != 0) {
                status = 200;
                logger.info("Successfully deleted an entity.");
            } else {
                status = 304;
                logger.info("No changes made to entity.");
            }
            connection.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
            status = 400;
        }
        return status;
    }
}
