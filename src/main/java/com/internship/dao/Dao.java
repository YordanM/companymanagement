package com.internship.dao;

import java.util.List;

/**
 * @author Yordan Marinov
 */
public interface Dao<E> {
    List<E> getAll();
    E getById(int id);
    int save(E toSave);
    int update(E toUpdate);
    int delete(E toDelete);
}
