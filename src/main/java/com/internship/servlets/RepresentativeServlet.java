package com.internship.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.internship.dao.CompanyDaoImpl;
import com.internship.pojo.Company;
import com.internship.pojo.Representative;
import com.internship.services.RepresentativeServiceImpl;
import com.internship.services.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author Yordan Marinov
 */
public class RepresentativeServlet extends HttpServlet {

    private Service<Representative> service = new RepresentativeServiceImpl();
    private static final Logger logger
            = LoggerFactory.getLogger(CompanyDaoImpl.class);

    @Override
    public void init() throws ServletException {
        super.init();
    }


    private Representative convertReqToObject(HttpServletRequest req) {
        Representative current = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            current = objectMapper.readValue(req.getReader(), Representative.class);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return current;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("application/json");
        int status = service.saveEntity(convertReqToObject(req));
        resp.setStatus(status);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();
        if (req.getParameter("representative_id") == null) {
            List<Representative> representativeList = service.getAllEntities();
            if (!representativeList.isEmpty()) {
                for (Representative rep : representativeList) {
                    out.println(rep);
                }
                resp.setStatus(200);
            } else {
                resp.setStatus(204);
            }
        } else {
            Representative fetched =
                    service.getEntityById(Integer.valueOf(req.getParameter("representative_id")));
            if (fetched != null) {
                out.println(fetched);
                resp.setStatus(200);
            } else
                resp.setStatus(204);
        }
        out.close();
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        service.updateEntity(convertReqToObject(req));
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        service.deleteEntity(convertReqToObject(req));
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
