package com.internship.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.internship.dao.CompanyDaoImpl;
import com.internship.pojo.Company;
import com.internship.services.CompanyServiceImpl;
import com.internship.services.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author Yordan Marinov
 */
public class CompanyServlet extends HttpServlet {

    private Service<Company> service = new CompanyServiceImpl();
    private static final Logger logger
            = LoggerFactory.getLogger(CompanyDaoImpl.class);

    @Override
    public void init() throws ServletException {
        super.init();
    }

    private Company convertReqToObject(HttpServletRequest req) {
        Company current = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            current = objectMapper.readValue(req.getReader(), Company.class);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return current;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("application/json");
        int status = service.saveEntity(convertReqToObject(req));
        resp.setStatus(status);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("application/json");
        ObjectMapper objectMapper = new ObjectMapper();
        PrintWriter out = resp.getWriter();
        if (req.getParameter("company_id") == null) {
            List<Company> companyList = service.getAllEntities();
            if (!companyList.isEmpty()) {
                    objectMapper.writeValue(out,companyList);
                resp.setStatus(200);
            } else {
                resp.setStatus(204);
            }
        } else {
            Company fetched =
                    service.getEntityById(Integer.valueOf(req.getParameter("company_id")));
            if (fetched != null) {
                objectMapper.writeValue(out,fetched);
                resp.setStatus(200);
            } else
                resp.setStatus(204);
        }
        out.close();
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int status = service.updateEntity(convertReqToObject(req));
        resp.setStatus(status);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int status = service.deleteEntity(convertReqToObject(req));
        resp.setStatus(status);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
